/*
 Navicat Premium Data Transfer

 Source Server         : LOCALHOST
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : geolocalizacion

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 25/03/2020 17:02:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for geolocalizacion
-- ----------------------------
DROP TABLE IF EXISTS `geolocalizacion`;
CREATE TABLE `geolocalizacion`  (
  `geo_id` int(11) NOT NULL AUTO_INCREMENT,
  `lat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lng` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fecha` datetime(0) NULL DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`geo_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 152 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of geolocalizacion
-- ----------------------------
INSERT INTO `geolocalizacion` VALUES (1, '4.6237856', '-74.1478278', '2020-03-25 22:38:47', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (2, '4.6238145', '-74.1478383', '2020-03-25 22:38:57', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (3, '4.6238262', '-74.1478499', '2020-03-25 22:39:07', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (4, '4.6238266', '-74.1478503', '2020-03-25 22:39:17', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (5, '4.6238195', '-74.1478531', '2020-03-25 22:39:27', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (6, '4.6238191', '-74.1478416', '2020-03-25 22:39:37', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (7, '4.623827', '-74.1478451', '2020-03-25 22:39:47', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (8, '4.6237875', '-74.1478311', '2020-03-25 22:39:57', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (9, '4.6238188', '-74.1478543', '2020-03-25 22:40:07', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (10, '4.6238308', '-74.1478593', '2020-03-25 22:40:17', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (11, '4.6238212', '-74.1478522', '2020-03-25 22:40:27', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (12, '4.6238221', '-74.1478735', '2020-03-25 22:40:37', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (13, '4.6238234', '-74.1478684', '2020-03-25 22:40:47', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (14, '4.6238193', '-74.147852', '2020-03-25 22:40:57', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (15, '4.623824', '-74.1478543', '2020-03-25 22:41:07', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (16, '4.623829', '-74.147857', '2020-03-25 22:41:17', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (17, '4.6238297', '-74.1478566', '2020-03-25 22:41:27', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (18, '4.6238296', '-74.1478602', '2020-03-25 22:41:37', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (19, '4.6238231', '-74.147855', '2020-03-25 22:41:47', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (20, '4.6238235', '-74.1478546', '2020-03-25 22:41:57', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (21, '4.6238077', '-74.1478346', '2020-03-25 22:42:07', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (22, '4.6238188', '-74.1478442', '2020-03-25 22:42:17', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (23, '4.623823', '-74.1478613', '2020-03-25 22:42:27', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (24, '4.6237942', '-74.1478379', '2020-03-25 22:42:37', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (25, '4.6238201', '-74.1478529', '2020-03-25 22:42:47', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (26, '4.6237751', '-74.1478398', '2020-03-25 22:42:57', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (27, '4.6237828', '-74.1478357', '2020-03-25 22:43:07', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (28, '4.6237626', '-74.1478208', '2020-03-25 22:43:17', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (29, '4.6237628', '-74.1478308', '2020-03-25 22:43:27', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (30, '4.6237546', '-74.147818', '2020-03-25 22:43:37', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (31, '4.6237605', '-74.1478284', '2020-03-25 22:43:47', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (32, '4.6237573', '-74.1478304', '2020-03-25 22:43:57', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (33, '4.6238085', '-74.1478415', '2020-03-25 22:44:07', 'f3xlXNHUMek:APA91bFeBZsdgbAwfWmom9zgkXl_rmN0j4tykyYiHPax0EzDDojrDET0TmectSRfgHr42axdSMoySe5ETtbMG7DQxodLFW1iVdw-4g-PR5ntLcUJQJHVYfw37bwG8gD4JYl50Ifs5jwe');
INSERT INTO `geolocalizacion` VALUES (34, '4.6238288', '-74.1478629', '2020-03-25 22:44:23', '');
INSERT INTO `geolocalizacion` VALUES (35, '4.623825', '-74.147862', '2020-03-25 22:44:34', '');
INSERT INTO `geolocalizacion` VALUES (36, '4.6238241', '-74.1478511', '2020-03-25 22:44:43', '');
INSERT INTO `geolocalizacion` VALUES (37, '4.6238123', '-74.1478315', '2020-03-25 22:44:53', '');
INSERT INTO `geolocalizacion` VALUES (38, '4.6238218', '-74.1478525', '2020-03-25 22:45:03', '');
INSERT INTO `geolocalizacion` VALUES (39, '4.6238301', '-74.1478651', '2020-03-25 22:45:13', '');
INSERT INTO `geolocalizacion` VALUES (40, '4.6238256', '-74.1478574', '2020-03-25 22:45:23', '');
INSERT INTO `geolocalizacion` VALUES (41, '4.6238249', '-74.1478485', '2020-03-25 22:45:33', '');
INSERT INTO `geolocalizacion` VALUES (42, '4.6238232', '-74.1478597', '2020-03-25 22:45:43', '');
INSERT INTO `geolocalizacion` VALUES (43, '4.6238307', '-74.1478391', '2020-03-25 22:45:54', '');
INSERT INTO `geolocalizacion` VALUES (44, '4.6238269', '-74.1478574', '2020-03-25 22:46:03', '');
INSERT INTO `geolocalizacion` VALUES (45, '4.6238258', '-74.147859', '2020-03-25 22:46:13', '');
INSERT INTO `geolocalizacion` VALUES (46, '4.6237779', '-74.1478191', '2020-03-25 22:46:23', '');
INSERT INTO `geolocalizacion` VALUES (47, '4.6237568', '-74.1478235', '2020-03-25 22:46:33', '');
INSERT INTO `geolocalizacion` VALUES (48, '4.623804', '-74.1478405', '2020-03-25 22:46:43', '');
INSERT INTO `geolocalizacion` VALUES (49, '4.6237678', '-74.1478091', '2020-03-25 22:46:53', '');
INSERT INTO `geolocalizacion` VALUES (50, '4.6238109', '-74.147842', '2020-03-25 22:47:03', '');
INSERT INTO `geolocalizacion` VALUES (51, '4.6238268', '-74.1478524', '2020-03-25 22:47:13', '');
INSERT INTO `geolocalizacion` VALUES (52, '4.6238321', '-74.1478584', '2020-03-25 22:47:23', '');
INSERT INTO `geolocalizacion` VALUES (53, '4.6238296', '-74.1478629', '2020-03-25 22:47:33', '');
INSERT INTO `geolocalizacion` VALUES (54, '4.6238196', '-74.1478444', '2020-03-25 22:47:43', '');
INSERT INTO `geolocalizacion` VALUES (55, '4.6238202', '-74.1478493', '2020-03-25 22:47:53', '');
INSERT INTO `geolocalizacion` VALUES (56, '4.6238252', '-74.1478488', '2020-03-25 22:48:03', '');
INSERT INTO `geolocalizacion` VALUES (57, '4.623814', '-74.1478381', '2020-03-25 22:48:13', '');
INSERT INTO `geolocalizacion` VALUES (58, '4.6238257', '-74.1478553', '2020-03-25 22:48:23', '');
INSERT INTO `geolocalizacion` VALUES (59, '4.6238385', '-74.1478164', '2020-03-25 22:48:33', '');
INSERT INTO `geolocalizacion` VALUES (60, '4.6238237', '-74.1478354', '2020-03-25 22:48:43', '');
INSERT INTO `geolocalizacion` VALUES (61, '4.623827', '-74.1478571', '2020-03-25 22:48:53', '');
INSERT INTO `geolocalizacion` VALUES (62, '4.6237405', '-74.1478255', '2020-03-25 22:49:03', '');
INSERT INTO `geolocalizacion` VALUES (63, '4.6237857', '-74.1478021', '2020-03-25 22:49:13', '');
INSERT INTO `geolocalizacion` VALUES (64, '4.6238188', '-74.1478314', '2020-03-25 22:49:23', '');
INSERT INTO `geolocalizacion` VALUES (65, '4.6238284', '-74.1478478', '2020-03-25 22:49:33', '');
INSERT INTO `geolocalizacion` VALUES (66, '4.623801', '-74.1478352', '2020-03-25 22:49:43', '');
INSERT INTO `geolocalizacion` VALUES (67, '4.6238149', '-74.147839', '2020-03-25 22:49:53', '');
INSERT INTO `geolocalizacion` VALUES (68, '4.6237728', '-74.1478337', '2020-03-25 22:50:03', '');
INSERT INTO `geolocalizacion` VALUES (69, '4.6237883', '-74.1478294', '2020-03-25 22:50:13', '');
INSERT INTO `geolocalizacion` VALUES (70, '4.6238236', '-74.1478554', '2020-03-25 22:50:23', '');
INSERT INTO `geolocalizacion` VALUES (71, '4.6238329', '-74.1478653', '2020-03-25 22:50:33', '');
INSERT INTO `geolocalizacion` VALUES (72, '4.6238296', '-74.1478553', '2020-03-25 22:50:43', '');
INSERT INTO `geolocalizacion` VALUES (73, '4.623799', '-74.1478333', '2020-03-25 22:50:53', '');
INSERT INTO `geolocalizacion` VALUES (74, '4.6237952', '-74.1478304', '2020-03-25 22:51:03', '');
INSERT INTO `geolocalizacion` VALUES (75, '4.623795', '-74.1478268', '2020-03-25 22:51:13', '');
INSERT INTO `geolocalizacion` VALUES (76, '4.6237948', '-74.1478313', '2020-03-25 22:51:23', '');
INSERT INTO `geolocalizacion` VALUES (77, '4.6237948', '-74.1478297', '2020-03-25 22:51:33', '');
INSERT INTO `geolocalizacion` VALUES (78, '4.6238146', '-74.1478481', '2020-03-25 22:51:43', '');
INSERT INTO `geolocalizacion` VALUES (79, '4.6238223', '-74.1478418', '2020-03-25 22:51:53', '');
INSERT INTO `geolocalizacion` VALUES (80, '4.6238231', '-74.1478409', '2020-03-25 22:52:03', '');
INSERT INTO `geolocalizacion` VALUES (81, '4.6238192', '-74.1478508', '2020-03-25 22:52:13', '');
INSERT INTO `geolocalizacion` VALUES (82, '4.6238264', '-74.1478607', '2020-03-25 22:52:23', '');
INSERT INTO `geolocalizacion` VALUES (83, '4.6238232', '-74.1478427', '2020-03-25 22:52:33', '');
INSERT INTO `geolocalizacion` VALUES (84, '4.6238215', '-74.147838', '2020-03-25 22:52:43', '');
INSERT INTO `geolocalizacion` VALUES (85, '4.6238212', '-74.1478414', '2020-03-25 22:52:53', '');
INSERT INTO `geolocalizacion` VALUES (86, '4.6238219', '-74.1478414', '2020-03-25 22:53:03', '');
INSERT INTO `geolocalizacion` VALUES (87, '4.6237986', '-74.1478319', '2020-03-25 22:53:13', '');
INSERT INTO `geolocalizacion` VALUES (88, '4.6238183', '-74.147844', '2020-03-25 22:53:23', '');
INSERT INTO `geolocalizacion` VALUES (89, '4.6238199', '-74.1478287', '2020-03-25 22:55:10', '');
INSERT INTO `geolocalizacion` VALUES (90, '4.6238199', '-74.1478287', '2020-03-25 22:55:10', '');
INSERT INTO `geolocalizacion` VALUES (91, '4.6238199', '-74.1478287', '2020-03-25 22:55:10', '');
INSERT INTO `geolocalizacion` VALUES (92, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (93, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (94, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (95, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (96, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (97, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (98, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (99, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (100, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (101, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (102, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (103, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (104, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (105, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (106, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (107, '4.6238199', '-74.1478287', '2020-03-25 22:55:11', '');
INSERT INTO `geolocalizacion` VALUES (108, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (109, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (110, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (111, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (112, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (113, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (114, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (115, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (116, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (117, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (118, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (119, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (120, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (121, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (122, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (123, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (124, '4.6238199', '-74.1478287', '2020-03-25 22:55:12', '');
INSERT INTO `geolocalizacion` VALUES (125, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (126, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (127, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (128, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (129, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (130, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (131, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (132, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (133, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (134, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (135, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (136, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (137, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (138, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (139, '4.6238199', '-74.1478287', '2020-03-25 22:55:13', '');
INSERT INTO `geolocalizacion` VALUES (140, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (141, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (142, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (143, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (144, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (145, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (146, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (147, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (148, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (149, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (150, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');
INSERT INTO `geolocalizacion` VALUES (151, '4.6238199', '-74.1478287', '2020-03-25 22:55:14', '');

SET FOREIGN_KEY_CHECKS = 1;
