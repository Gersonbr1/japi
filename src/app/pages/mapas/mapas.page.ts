import { Component, OnInit, NgZone } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';
import { LoadingController } from '@ionic/angular'
import { EnviodatosService } from 'src/app/service/enviodatos.service';
import { FCM } from '@ionic-native/fcm/ngx';

declare var google;

@Component({
  selector: 'app-mapas',
  templateUrl: './mapas.page.html',
  styleUrls: ['./mapas.page.scss'],
})
export class MapasPage implements OnInit {

  mapRef = null;
  latitud;
  longitud;
  token;

  constructor(
    private geolocation: Geolocation
    ,private loadingCtrl : LoadingController
    ,private geocoder : NativeGeocoder
    ,private ngZone : NgZone
    ,private enviodatos : EnviodatosService
    ,private fcm : FCM
  ) { 
    this.fcm.getToken().then(token => {
      this.token = token;
    })

    
  }


  ngOnInit() {
    this.loadMap();
    setInterval(() => { 
      this.getLocation()
    },10000)
  }
  async loadMap(){

    const loading = await this.loadingCtrl.create();
    loading.present();
    const myLatLng = await this.getLocation();
    const mapEle: HTMLElement = document.getElementById('map');
    this.mapRef = new google.maps.Map(mapEle,{
      center: myLatLng
      ,zoom: 5
    })
    // Saber cuando se termina la ejecución del mapa
    google.maps.event.addListenerOnce(this.mapRef,'idle', () => {

      var url = "zonasegura/api_zonas/json.php";
      this.enviodatos.SenDataPost(url,{lat: this.latitud, lng : this.longitud,token : this.token}).then((result)=>{
        for(let data of result['zonas']){
          let lati:number = data['lat'] * 1;
          let long:number = data['lng'] * 1;
          this.addMarker(lati,long)
        }

        // this.addMarker(myLatLng.lat,myLatLng.lng)
         loading.dismiss();

      })
    })
  }

  
 
  private addMarker(lat:number,lng: number){

    console.log(lat+"***"+lng);

    const marker = new google.maps.Marker({
      position: {lat : lat ,lng : lng }
      ,map : this.mapRef
      ,title: 'Hola Mapa'
      // ,animation: google.maps.Animation.BOUNCE
    })
  }

  private async getLocation(){
    const rta = await this.geolocation.getCurrentPosition();
    // this.reverseGeocoding(rta.coords.latitude,rta.coords.longitude);
    this.latitud = rta.coords.latitude
    this.longitud = rta.coords.longitude;

    this.enviodatos.SenDataPost("zonasegura/conexion_2.php",{lat: this.latitud, lng : this.longitud,token : this.token}).then((result)=>{
        if(result['zonaSegura'] == 1){
          this.enviodatos.toast("Estas en una zona insegura");
        }
    })
    
    return {lat: this.latitud,lng: this.longitud }
  }

}
