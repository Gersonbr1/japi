import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingController, AlertController, NavController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class EnviodatosService {
  public urlServer = "https://somosunoqroo.org/";
  public loading: any;
  constructor(
    private http: HttpClient,
    public loadingCtrl: LoadingController,
    public toastController: ToastController
  ) { }



  SenDataPost(url: string, params: any) {

    return new Promise((resolve, reject) => {
      let response = this.http.post(this.urlServer + url, params, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
      response.subscribe((data) => {
        resolve(data);
      }, err => {
        this.toast('Error al realizar la conexión')
        console.log("error here #1?");
      });

    });
  }
  toast(mensaje) {
    const toast = this.toastController.create({
      message: mensaje,
      duration: 2000
    }).then(alert => alert.present());

    //this.ToastController.create({message:mensaje,duration:3000})
  }
}
